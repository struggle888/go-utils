package api

import (
	"context"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"net/http"
	"time"
)

var (
	MongoConn                 *mongo.Client
	MongoDatabase             string
	AccessTokenValidDuration  = time.Hour
	RefreshTokenValidDuration = 24 * time.Hour
	Secret                    []byte
)

type LoginParam struct {
	Username string `form:"username" json:"username" uri:"username" xml:"username" binding:"required"`
	Password string `form:"password" json:"password" uri:"password" xml:"password" binding:"required"`
}

func Login(c *gin.Context) {
	form := new(LoginParam)
	if err := c.Bind(form); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"state": 903,
			"msg":   "invalid params",
		})
		return
	}
	userId, err := CheckAuth(form)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"state": 998,
			"msg":   "server internal error",
		})
		return
	}
	if userId == "" {
		c.JSON(http.StatusUnauthorized, gin.H{
			"state": 900,
			"msg":   "authentication failed",
		})
		return
	}

	accessToken, refreshToken := GenerateToken(userId)
	c.JSON(http.StatusOK, gin.H{
		"state": 800,
		"payload": gin.H{
			"access_token":  accessToken,
			"refresh_token": refreshToken,
		},
	})
}

func RefreshAccessToken(c *gin.Context) {
	token := c.GetHeader("token")

	refreshToken, err := NewToken(token)
	if err != nil || refreshToken.tokenType != "r" {
		c.JSON(http.StatusForbidden, gin.H{
			"state": 900,
			"msg":   "authentication failed",
		})
		return
	}

	if refreshToken.expireTime < time.Now().Unix() {
		c.JSON(http.StatusForbidden, gin.H{
			"state": 902,
			"msg":   "expired refresh token",
		})
		return
	}
	c.JSON(http.StatusOK, gin.H{
		"state": 800,
		"payload": gin.H{
			"access_token": Token{
				tokenType:  "a",
				userId:     refreshToken.userId,
				pass:       refreshToken.pass,
				expireTime: time.Now().Add(AccessTokenValidDuration).Unix(),
			}.Pack(),
		},
	})
}

func CheckAuth(form *LoginParam) (string, error) {
	collection := MongoConn.Database(MongoDatabase).Collection("user")
	result := make(map[string]string)
	err := collection.FindOne(context.TODO(), bson.D{{"username", form.Username}, {"password", form.Password}}, options.FindOne().SetProjection(bson.M{"_id": 0, "user_id": 1})).Decode(result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return "", nil
		}
		return "", err
	}
	return result["user_id"], nil
}

func CheckAccessToken(c *gin.Context) bool {
	token := c.GetHeader("token")
	if token == "" {
		c.JSON(http.StatusUnauthorized, gin.H{
			"state": 900,
			"msg":   "authentication failed",
		})
		return false
	}

	accessToken, err := NewToken(token)
	if err != nil || accessToken.tokenType != "a" {
		c.JSON(http.StatusForbidden, gin.H{
			"state": 900,
			"msg":   "authentication failed",
		})
		return false
	}

	if accessToken.expireTime < time.Now().Unix() {
		c.JSON(http.StatusForbidden, gin.H{
			"state": 901,
			"msg":   "expired access token",
		})
		return false
	}

	return true
}
