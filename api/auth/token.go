package api

import (
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/wumansgy/goEncrypt"
	"math/rand"
	"strconv"
	"strings"
	"time"
)

type Token struct {
	tokenType  string
	userId     string
	pass       string
	expireTime int64
}

func (t Token) Pack() string {
	c, err := goEncrypt.AesCbcEncrypt([]byte(fmt.Sprintf("%s %s %s %x", t.tokenType, t.userId, t.pass, t.expireTime)), Secret, []byte("Druggle in hwtih"))
	if err != nil {
		panic(fmt.Errorf("error in encrypt aes: %v", err))
	}
	return base64.RawURLEncoding.EncodeToString(c)
}

func NewToken(token string) (t Token, err error) {
	c, err := base64.RawURLEncoding.DecodeString(token)
	if err != nil {
		return t, err
	}
	b, err := goEncrypt.AesCbcDecrypt(c, Secret, []byte("Druggle in hwtih"))
	if err != nil {
		return t, err
	}
	parts := strings.Split(string(b), " ")
	if len(parts) != 4 {
		return t, errors.New("error in unpack token, except length 4")
	}
	t.tokenType = parts[0]
	t.userId = parts[1]
	t.pass = parts[2]
	t.expireTime, err = strconv.ParseInt(parts[3], 16, 64)
	if err != nil {
		return t, err
	}
	return t, nil
}

func GenerateToken(userId string) (string, string) {
	pass := strconv.Itoa(rand.Int())
	return Token{
			tokenType:  "a",
			userId:     userId,
			pass:       pass,
			expireTime: time.Now().Add(AccessTokenValidDuration).Unix(),
		}.Pack(), Token{
			tokenType:  "r",
			userId:     userId,
			pass:       pass,
			expireTime: time.Now().Add(RefreshTokenValidDuration).Unix(),
		}.Pack()
}
