package tools

type StringRectifier struct {
	cache     []string
	BatchSize int
	BatchFunc func(s []string)
}

func (r *StringRectifier) Add(s string) {
	r.cache = append(r.cache, s)
	if len(r.cache) >= r.BatchSize {
		r.BatchFunc(r.cache)
		r.cache = nil
	}
}

func (r *StringRectifier) AddSlice(s []string) {
	r.cache = append(r.cache, s...)
	if len(r.cache) >= r.BatchSize {
		r.BatchFunc(r.cache)
		r.cache = nil
	}
}

func (r *StringRectifier) FlushCache() {
	r.BatchFunc(r.cache)
	r.cache = nil
}

func (r *StringRectifier) ClearCache() {
	r.cache = nil
}
