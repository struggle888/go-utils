/*
常用工具函数集
*/
package tools

/*
零散的工具方法
*/
import (
	"encoding/binary"
	"golang.org/x/net/idna"
	"io/ioutil"
	"net"
	"os"
	"strings"
	"sync"
)

// LockMap 带有读写锁的Map, 用于众多并发场景
type LockMap struct {
	sync.RWMutex
	Map map[uint16]int
}

// IP2Int ip地址转化为uint32
func IP2Int(ip string) uint32 {
	return binary.BigEndian.Uint32(net.ParseIP(ip)[12:16])
}

// Int2IP uint32转化为ip字符串
func Int2IP(nn uint32) string {
	ip := make(net.IP, 4)
	binary.BigEndian.PutUint32(ip, nn)
	return ip.String()
}

// StringArrayContains 判断字符串数组中是否含有目标字符串
func StringArrayContains(item string, array []string) bool {
	for _, i := range array {
		if i == item {
			return true
		}
	}
	return false
}

// IsFileExist 目的路径/文件是否存在
func IsFileExist(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

// CidrToIpString CIDR地址展开为IP字符串列表，注意不要爆内存
func CidrToIpString(cidr string) (ips []string) {
	_, ipv4Net, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil
	}
	mask := binary.BigEndian.Uint32(ipv4Net.Mask)
	start := binary.BigEndian.Uint32(ipv4Net.IP)
	finish := (start & mask) | (mask ^ 0xffffffff)

	for i := start; i <= finish; i++ {
		ip := make(net.IP, 4)
		binary.BigEndian.PutUint32(ip, i)
		ips = append(ips, ip.String())
	}
	return ips
}

// CidrToIp CIDR地址展开为net.IP数组，注意不要爆内存
func CidrToIp(cidr string) (ips []net.IP) {
	_, ipv4Net, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil
	}
	mask := binary.BigEndian.Uint32(ipv4Net.Mask)
	start := binary.BigEndian.Uint32(ipv4Net.IP)
	finish := (start & mask) | (mask ^ 0xffffffff)

	for i := start; i <= finish; i++ {
		ip := make(net.IP, 4)
		binary.BigEndian.PutUint32(ip, i)
		ips = append(ips, ip)
	}
	return ips
}

// CheckSum 计算checksum值，用于IP头和UDP头部校验和的计算
func CheckSum(data []byte) uint16 {
	var (
		sum    uint32
		length = len(data)
		index  int
	)
	for length > 1 {
		sum += uint32(data[index])<<8 + uint32(data[index+1])
		index += 2
		length -= 2
	}
	if length > 0 {
		sum += uint32(data[index]) << 8
	}
	for sum>>16 != 0 {
		sum = (sum & 0xffff) + (sum >> 16)
	}
	return uint16(^sum)
}

// ReadStringsFromFile 把目标文件读成字符串数组
func ReadStringsFromFile(filename string) ([]string, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return strings.Split(strings.TrimRight(strings.ReplaceAll(string(b), "\r", ""), "\n"), "\n"), nil
}

var IntranetIPs = [][]uint32{
	{167772160, 184549375},   // 10.0.0.0/8
	{2130706432, 2147483647}, // 127.0.0.0/8
	{2886729728, 2887778303}, // 172.16.0.0/12
	{3232235520, 3232301055}, // 192.168.0.0/16
}

var NotPublicIPs = append(IntranetIPs, [][]uint32{
	{0, 16777215},            // 0.0.0.0/8
	{1681915904, 1686110207}, // 100.64.0.0/10
	{2851995648, 2852061183}, // 169.254.0.0/16
	{3221225472, 3221225727}, // 192.0.0.0/24
	{3221225984, 3221226239}, // 192.0.2.0/24
	{3227017984, 3227018239}, // 192.88.99.0/24
	{3323068416, 3323199487}, // 198.18.0.0/15
	{3325256704, 3325256959}, // 198.51.100.0/24
	{3405803776, 3405804031}, // 203.0.113.0/24
	{4026531840, 4294967295}, // 240.0.0.0/4
}...)

func IsIntranetIP(ip string) bool {
	n := IP2Int(ip)
	for i := range IntranetIPs {
		if n >= IntranetIPs[i][0] && n <= IntranetIPs[i][1] {
			return true
		}
	}
	return false
}

func IsPublicIP(ip string) bool {
	n := IP2Int(ip)
	for i := range NotPublicIPs {
		if n >= NotPublicIPs[i][0] && n <= NotPublicIPs[i][1] {
			return false
		}
	}
	return true
}

// DomainA2U convert ascii to unicode(xn--wmqs6w.xn--fiqs8s -> 今天.中国)
func DomainA2U(domain string) string {
	s, err := idna.New().ToUnicode(domain)
	if err != nil {
		return domain
	} else {
		return s
	}
}

// DomainU2A convert unicode to ascii (今天.中国 -> xn--wmqs6w.xn--fiqs8s)
func DomainU2A(domain string) string {
	s, err := idna.New().ToASCII(domain)
	if err != nil {
		return domain
	} else {
		return s
	}
}
