package shuffler

import (
	"fmt"
	"math/rand"
	"strconv"
	"testing"
)

func ExampleNewShuffler() {
	shuffler := NewShuffler(10, []byte("any seed"))
	var i uint32
	for i = 0; i < shuffler.Size(); i++ {
		fmt.Println(shuffler.Get(i))
	}

	// Output:
	// 0
	// 7
	// 5
	// 9
	// 6
	// 1
	// 8
	// 2
	// 4
	// 3
}

func ExampleShuffler_Get() {
	ips := []string{"1.2.3.4", "5.6.7.8", "9.10.11.12"}
	ports := []uint16{22, 80, 443, 3389}
	size := uint32(len(ips) * len(ports))
	shuffler := NewShuffler(size, []byte("any seed"))
	var i uint32
	for i = 0; i < size; i++ {
		n := int(shuffler.Get(i))
		ip := ips[n/len(ports)]
		port := ports[n%len(ports)]
		fmt.Printf("%s:%d\n", ip, port)
	}

	// Output:
	// 9.10.11.12:443
	// 5.6.7.8:3389
	// 5.6.7.8:80
	// 9.10.11.12:80
	// 5.6.7.8:443
	// 1.2.3.4:80
	// 9.10.11.12:3389
	// 1.2.3.4:443
	// 5.6.7.8:22
	// 1.2.3.4:3389
	// 1.2.3.4:22
	// 9.10.11.12:22
}

func BenchmarkShuffler(b *testing.B) {
	shuffler := NewShuffler(uint32(b.N), []byte(strconv.Itoa(rand.Intn(4000000))))
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		shuffler.Get(uint32(i))
	}
}

func BenchmarkShuffle(b *testing.B) {
	rand.Perm(b.N)
}
