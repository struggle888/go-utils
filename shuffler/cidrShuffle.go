package shuffler

import (
	"bitbucket.org/struggle888/go-utils/tools"
	"errors"
	"fmt"
	"math/big"
	"math/rand"
	"net"
	"sort"
	"strings"
)

type IPSet interface {
	Size() uint32
	Get(uint32) string
}

type cidr struct {
	base uint32
	size uint32
}

func (c *cidr) Size() uint32 {
	return c.size
}

func (c *cidr) Get(index uint32) string {
	return tools.Int2IP(c.base + index)
}

type cidr6 struct {
	base *big.Int
	size uint32
}

func (c *cidr6) Size() uint32 {
	return c.size
}

func (c *cidr6) Get(index uint32) string {
	return IntToIp6(big.NewInt(0).Add(c.base, big.NewInt(int64(index))))
}

type ScatteredIP []string

func (ipSet *ScatteredIP) Add(s string) {
	*ipSet = append(*ipSet, s)
}

func (ipSet *ScatteredIP) Size() uint32 {
	return uint32(len(*ipSet))
}

func (ipSet *ScatteredIP) Get(index uint32) string {
	return (*ipSet)[int(index)]
}

// GlobalCIDRs 全部公网IP合并后的CIDR地址列表
var GlobalCIDRs = []string{"1.0.0.0/8", "2.0.0.0/7", "4.0.0.0/6", "8.0.0.0/7", "11.0.0.0/8", "12.0.0.0/6", "16.0.0.0/4", "32.0.0.0/3", "64.0.0.0/3", "96.0.0.0/4", "112.0.0.0/5", "120.0.0.0/6", "124.0.0.0/7", "126.0.0.0/8", "128.0.0.0/3", "160.0.0.0/5", "168.0.0.0/6", "172.0.0.0/12", "172.32.0.0/11", "172.64.0.0/10", "172.128.0.0/9", "173.0.0.0/8", "174.0.0.0/7", "176.0.0.0/4", "192.0.0.0/9", "192.128.0.0/11", "192.160.0.0/13", "192.169.0.0/16", "192.170.0.0/15", "192.172.0.0/14", "192.176.0.0/12", "192.192.0.0/10", "193.0.0.0/8", "194.0.0.0/7", "196.0.0.0/6", "200.0.0.0/5", "208.0.0.0/4"}

// CIDRsShuffler 面向多个CIDR地址的全随机IP生成器, 单次生成IP所需时间: 200ns
// 注意: 此生成器不会将输入的多个CIDR地址去重或合并
type CIDRsShuffler struct {
	ipSets    []IPSet
	sumIndex  []uint32
	totalSize uint32
	shuffler  *Shuffler

	ipCache *ScatteredIP
}

/*
NewCIDRsShuffler
参数cidrs为全随机使用的CIDR地址列表，seed为随机种子（使用同一个种子产生出的全随机序列是相同的，可用于程序暂停和继续）
*/
func NewCIDRsShuffler(cidrs []string, seed []byte) (*CIDRsShuffler, error) {
	cidrsShuffler := &CIDRsShuffler{
		ipSets:    []IPSet{},
		totalSize: 0,
	}
	var size uint32
	for _, c := range cidrs {
		split := strings.Index(c, "/")
		// is cidr
		if split != -1 {
			_, cidrNet, err := net.ParseCIDR(c)
			if err != nil {
				return nil, fmt.Errorf("unrecognized network string: %s", c)
			}
			ones, bits := cidrNet.Mask.Size()
			if bits-ones > 32 {
				return nil, fmt.Errorf("too large cidr size: %s", c)
			}
			size = 1 << (bits - ones)
			// is ipv6 cidr
			if cidrNet.IP.To4() == nil {
				cidrsShuffler.ipSets = append(cidrsShuffler.ipSets, &cidr6{
					base: Ip6ToInt(cidrNet.IP),
					size: 1 << (bits - ones),
				})
			} else { // is ipv4
				cidrsShuffler.ipSets = append(cidrsShuffler.ipSets, &cidr{
					base: tools.IP2Int(c[:split]),
					size: size,
				})
			}
			if cidrsShuffler.totalSize+size < cidrsShuffler.totalSize {
				return nil, fmt.Errorf("ip amount out of range")
			}
			cidrsShuffler.totalSize += size
			// is ip range
		} else if strings.Index(c, "-") != -1 {
			parts := strings.Split(c, "-")
			if len(parts) != 2 {
				panic(fmt.Sprintf("Unrecognized network string: %s", c))
			}
			startIP := net.ParseIP(parts[0])
			startIPv4 := startIP.To4()
			endIP := net.ParseIP(parts[1])
			endIPv4 := endIP.To4()
			if startIP == nil || endIP == nil || (startIPv4 == nil && endIPv4 != nil) || (startIPv4 != nil && endIPv4 == nil) {
				return nil, fmt.Errorf("unrecognized network string: %s", c)
			}
			// is ipv6
			if startIPv4 == nil {
				start := Ip6ToInt(startIP)
				end := Ip6ToInt(endIP)
				if end.Cmp(start) < 0 {
					return nil, fmt.Errorf("unrecognized network string: %s", c)
				}
				s := big.NewInt(0).Sub(end, start)
				if !s.IsUint64() || s.Uint64() > ((1<<32)-2) {
					return nil, fmt.Errorf("too large ip range: %s", c)
				}
				size = uint32(s.Uint64()) + 1
				cidrsShuffler.ipSets = append(cidrsShuffler.ipSets, &cidr6{
					base: start,
					size: size,
				})
			} else { // is ipv4
				start := tools.IP2Int(parts[0])
				end := tools.IP2Int(parts[1])
				if end < start {
					return nil, fmt.Errorf("unrecognized network string: %s", c)
				}
				size = end - start + 1
				cidrsShuffler.ipSets = append(cidrsShuffler.ipSets, &cidr{
					base: start,
					size: end - start + 1,
				})
			}
			if cidrsShuffler.totalSize+size < cidrsShuffler.totalSize {
				return nil, fmt.Errorf("ip amount out of range")
			}
			cidrsShuffler.totalSize += size
			// 点IP
		} else if ip := net.ParseIP(c); ip != nil {
			if cidrsShuffler.ipCache == nil {
				cidrsShuffler.ipCache = &ScatteredIP{}
				cidrsShuffler.ipSets = append(cidrsShuffler.ipSets, cidrsShuffler.ipCache)
			}
			cidrsShuffler.ipCache.Add(c)
			if cidrsShuffler.totalSize+1 < cidrsShuffler.totalSize {
				return nil, fmt.Errorf("ip amount out of range")
			}
			cidrsShuffler.totalSize += 1
		} else {
			return nil, fmt.Errorf("unrecognized network string: %s", c)
		}
	}

	// buildIndex
	cidrsShuffler.sumIndex = make([]uint32, len(cidrsShuffler.ipSets)+1)
	var cumulativeSum uint32
	for i, s := range cidrsShuffler.ipSets {
		cumulativeSum += s.Size()
		cidrsShuffler.sumIndex[i+1] = cumulativeSum
	}

	cidrsShuffler.shuffler = NewShuffler(cidrsShuffler.totalSize, seed)
	return cidrsShuffler, nil
}

// Get 创建好CIDRsShuffler后可以将其理解为一个已经打乱后的全随机IP列表，此方法为取出列表中对应索引的值, 所以i不可大于CIDRsShuffler的Size
func (shuffler *CIDRsShuffler) Get(i uint32) string {
	index := shuffler.shuffler.Get(i)
	ii := shuffler.search(index)
	if ii < len(shuffler.ipSets) {
		index -= shuffler.sumIndex[ii]
		return shuffler.ipSets[ii].Get(index)
	}
	panic(errors.New("shuffler index must less than size"))
	//index := shuffler.shuffler.Get(i)
	//for i := range shuffler.ipSets {
	//	if index >= shuffler.ipSets[i].Size() {
	//		index -= shuffler.ipSets[i].Size()
	//	} else {
	//		return shuffler.ipSets[i].Get(index)
	//	}
	//}
	//panic(errors.New("shuffler index must less than size"))
}

// GetRange 获取一定Index范围内的结果，[from, to) 不包含to
// panic if from >= to
func (shuffler *CIDRsShuffler) GetRange(from, to uint32) []string {
	if from >= to {
		panic(errors.New("from >= to"))
	}
	indexes := make([]uint32, to-from)
	for i := from; i < to; i++ {
		indexes[i-from] = shuffler.shuffler.Get(i)
	}
	sort.Slice(indexes, func(i, j int) bool {
		return indexes[i] < indexes[j]
	})
	result := make([]string, to-from)
	currentIndex := 0
	var passed uint32
	for i := range shuffler.ipSets {
		for indexes[currentIndex]-passed < shuffler.ipSets[i].Size() {
			result[currentIndex] = shuffler.ipSets[i].Get(indexes[currentIndex] - passed)
			currentIndex++
			if currentIndex == len(indexes) {
				rand.Shuffle(len(result), func(i, j int) {
					result[i], result[j] = result[j], result[i]
				})
				return result
			}
		}
		passed += shuffler.ipSets[i].Size()
	}
	panic(errors.New("shuffler index must less than size"))
}

// Size 获取CIDRsShuffler的容量
func (shuffler *CIDRsShuffler) Size() uint32 {
	return shuffler.totalSize
}

func (shuffler *CIDRsShuffler) search(index uint32) int {
	lo, hi := 0, len(shuffler.sumIndex)-1
	for lo < hi {
		mid := lo + (hi-lo)/2
		if shuffler.sumIndex[mid] <= index {
			lo = mid + 1
		} else {
			hi = mid
		}
	}
	return lo - 1

}

func IntToIp6(i *big.Int) string {
	b := i.Bytes()
	return net.IP(append(make([]byte, 16-len(b)), b...)).String()
}

func Ip6ToInt(ip net.IP) *big.Int {
	return big.NewInt(0).SetBytes(ip)
}
