package shuffler

import (
	"fmt"
	"testing"
	"time"
)

func TestCIDRsShuffler_Get(t *testing.T) {
	shuffler, err := NewCIDRsShuffler([]string{"192.168.1.0/30", "172.1.1.0/30", "1.2.3.0/29", "2.2.3.4-2.2.3.8", "6.7.8.9", "9.9.9.9"}, []byte("any seed"))
	if err != nil {
		panic(err)
	}
	var i uint32
	for i = 0; i < shuffler.Size(); i++ {
		fmt.Println(shuffler.Get(i))
	}
}
func TestCIDRsShuffler_GetRange(t *testing.T) {
	shuffler, err := NewCIDRsShuffler([]string{"2200:1::/125", "2222::0-2222::10", "2333::1", "192.168.1.0/30", "172.1.1.0/30", "1.2.3.0/29", "2.2.3.4-2.2.3.8", "6.7.8.9", "9.9.9.9"}, []byte("any seed"))
	if err != nil {
		panic(err)
	}
	fmt.Println(shuffler.GetRange(0, 3))
}

func BenchmarkCIDRsShuffler(b *testing.B) {
	var s []string
	var s1 = []string{"1.2.3.0/30", "2.3.4.0/30"}
	for i := 0; i < 1000000; i++ {
		s = append(s, s1...)
	}
	shuffler, err := NewCIDRsShuffler(s, []byte(time.Now().String()))
	if err != nil {
		panic(err)
	}
	b.ResetTimer()
	var i uint32
	for i = 0; int(i) < b.N; i++ {
		shuffler.Get(i % shuffler.Size())
	}
}
