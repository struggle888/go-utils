package shuffler

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/sha512"
	"encoding/binary"
	"errors"
	"math"
)

// Shuffler 随机序列生成器
// 生成指定范围0-size数字的全随机序列
// 平均每次生成(Get)花费170ns
type Shuffler struct {
	size    uint32
	seed    []byte
	keys    []cipher.Block
	element uint32
}

// NewShuffler 初始化一个Shuffler
// size: 指定随机容量
// seed：指定随机种子(相同种子产出的随机序列是相同的)
func NewShuffler(size uint32, seed []byte) *Shuffler {
	shuffler := Shuffler{
		size:    size,
		seed:    seed,
		element: uint32(math.Ceil(math.Sqrt(float64(size)))),
	}
	keys := sha512.Sum512(seed)
	block1, _ := aes.NewCipher(keys[:16])
	block2, _ := aes.NewCipher(keys[16:32])
	block3, _ := aes.NewCipher(keys[32:48])
	shuffler.keys = []cipher.Block{
		block1, block2, block3,
	}
	return &shuffler
}

// Get 获取对应序号的随机值
// 创建好Shuffler后可以将其理解为一个已经打乱后的数字列表，此方法为取出列表中对应索引的值
// 注意: index不可大于Shuffler的Size
func (shuffler *Shuffler) Get(index uint32) uint32 {
	if index >= shuffler.size {
		panic(errors.New("shuffler index must less than size"))
	}
	return shuffler.encrypt(index)
}

// Size 获取Shuffler的容量(size)
func (shuffler *Shuffler) Size() uint32 {
	return shuffler.size
}

// 加密算法: Ciphers with Arbitrary Finite Domains(https://web.cs.ucdavis.edu/~rogaway/papers/subset.pdf)
func (shuffler *Shuffler) encrypt(index uint32) uint32 {
	for {
		index = shuffler._encrypt(index)
		if index < shuffler.size {
			return index
		}
	}
}

// 执行一次加密计算
func (shuffler *Shuffler) _encrypt(t uint32) uint32 {
	l := t % shuffler.element
	r := t / shuffler.element
	for _, key := range shuffler.keys {
		tmp := (l + aesRandomize(key, r)) % shuffler.element
		l = r
		r = tmp
	}
	return shuffler.element*l + r
}

// aes加密一个块
func aesRandomize(key cipher.Block, value uint32) uint32 {
	dst := make([]byte, 16)
	binary.LittleEndian.PutUint32(dst, value)
	key.Encrypt(dst, dst)
	return binary.BigEndian.Uint32(dst)
}
