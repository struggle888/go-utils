package dns

import (
	"github.com/miekg/dns"
	"net"
	"strings"
	"sync"
)

// SimpleDNSTool 定义了DNS Client池和Message池，为了实现高并发情况下资源的高效复用
type SimpleDNSTool struct {
	clientPool *sync.Pool
	msgPool    *sync.Pool
}

func NewSimpleDNS() *SimpleDNSTool {
	return &SimpleDNSTool{
		clientPool: &sync.Pool{New: func() interface{} { return new(dns.Client) }},
		msgPool:    &sync.Pool{New: func() interface{} { return new(dns.Msg) }},
	}
}

// GetARecord 使用server去查询domain的A记录
func (tool *SimpleDNSTool) GetARecord(domain string, server string) (ips []net.IP) {
	// 从池中取对象来实现资源复用
	client := tool.clientPool.Get().(*dns.Client)
	defer tool.clientPool.Put(client)
	msg := tool.msgPool.Get().(*dns.Msg)
	defer tool.clientPool.Put(msg)

	if !strings.Contains(server, ":") {
		server += ":53"
	}
	// 设置问题
	msg.SetQuestion(dns.Fqdn(domain), dns.TypeA)
	resp, _, err := client.Exchange(msg, server)
	if err != nil || resp.Rcode != dns.RcodeSuccess {
		return
	}
	//解析每个结果
	for _, answer := range resp.Answer {
		if answer.Header().Rrtype == dns.TypeA {
			if r, ok := answer.(*dns.A); ok && r.Header().Name == dns.Fqdn(domain) {
				ips = append(ips, r.A)
			}
		}
	}
	return
}
