package dns

import (
	"context"
	"net"
)

type ManualDnsResolver struct {
	AddrMap  map[string][]net.IPAddr
	resolver *net.Resolver
}

func NewManualDnsResolver() *ManualDnsResolver {
	return &ManualDnsResolver{AddrMap: make(map[string][]net.IPAddr), resolver: &net.Resolver{}}
}

func (resolver *ManualDnsResolver) Add(domain string, ip net.IP) {
	resolver.AddrMap[domain] = append(resolver.AddrMap[domain], net.IPAddr{IP: ip})
}

func (resolver *ManualDnsResolver) Set(domain string, ip net.IP) {
	resolver.AddrMap[domain] = []net.IPAddr{{IP: ip}}
}

func (resolver *ManualDnsResolver) Remove(domain string) {
	delete(resolver.AddrMap, domain)
}

func (resolver *ManualDnsResolver) Clear() {
	resolver.AddrMap = make(map[string][]net.IPAddr)
}

// LookupIPAddr 适配fasthttp
func (resolver *ManualDnsResolver) LookupIPAddr(ctx context.Context, domain string) ([]net.IPAddr, error) {
	if name, has := resolver.AddrMap[domain]; has {
		return name, nil
	} else {
		return resolver.resolver.LookupIPAddr(ctx, domain)
	}
}
