package protocol

type TCPHeader struct {
	SrcPort   uint16
	DstPort   uint16
	SeqNumber uint32
	AckNumber uint32
	HeaderLen uint8
	// Flags
	NsFlag  bool
	CwrFlag bool
	EcnFlag bool
	UrgFlag bool
	AckFlag bool
	PshFlag bool
	RstFlag bool
	SynFlag bool
	FinFlag bool

	Window        uint16
	CheckSum      uint16
	UrgentPointer uint16

	Options []byte
}

func (header TCPHeader) Pack() []byte {
	if header.HeaderLen > 63 || header.HeaderLen%4 != 0 {
		panic("Illegal header length")
	}
	if header.HeaderLen == 0 {
		if len(header.Options)%4 != 0 {
			header.Options = append(header.Options, make([]byte, 4-(len(header.Options)%4))...)
		}
		if len(header.Options) > 40 {
			panic("Header Options too long")
		}
		header.HeaderLen = uint8(20 + len(header.Options))
	}
	base := make([]byte, 20)
	base[0], base[1] = byte(header.SrcPort>>8), byte(header.SrcPort&0xff)
	base[2], base[3] = byte(header.DstPort>>8), byte(header.DstPort&0xff)
	base[4], base[5], base[6], base[7] = byte((header.SeqNumber>>24)&0xff), byte((header.SeqNumber>>16)&0xff), byte((header.SeqNumber>>8)&0xff), byte(header.SeqNumber&0xff)
	base[8], base[9], base[10], base[11] = byte((header.AckNumber>>24)&0xff), byte((header.AckNumber>>16)&0xff), byte((header.AckNumber>>8)&0xff), byte(header.AckNumber&0xff)

	lenAndFlags := uint16(header.HeaderLen) << 10
	if header.FinFlag {
		lenAndFlags += 0x01
	}
	if header.SynFlag {
		lenAndFlags += 0x02
	}
	if header.RstFlag {
		lenAndFlags += 0x04
	}
	if header.PshFlag {
		lenAndFlags += 0x08
	}
	if header.AckFlag {
		lenAndFlags += 0x10
	}
	if header.UrgFlag {
		lenAndFlags += 0x20
	}
	if header.EcnFlag {
		lenAndFlags += 0x40
	}
	if header.CwrFlag {
		lenAndFlags += 0x80
	}
	if header.NsFlag {
		lenAndFlags += 0x0100
	}
	base[12], base[13] = byte(lenAndFlags>>8), byte(lenAndFlags&0xff)
	base[14], base[15] = byte(header.Window>>8), byte(header.Window&0xff)
	base[16], base[17] = byte(header.CheckSum>>8), byte(header.CheckSum&0xff)
	base[18], base[19] = byte(header.UrgentPointer>>8), byte(header.UrgentPointer&0xff)

	return append(base, header.Options...)
}
