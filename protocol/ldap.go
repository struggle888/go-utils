package protocol

import (
	"fmt"
	"github.com/go-ldap/ldap"
	"net/url"
	"strings"
)

type LDAPConfig struct {
	Scheme     string
	Host       string
	DN         string
	Scope      int
	Attributes []string
	Filter     string
	Extensions []string
}

var scopeMatch = map[string]int{
	"base": ldap.ScopeBaseObject,
	"one":  ldap.ScopeSingleLevel,
	"sub":  ldap.ScopeWholeSubtree,
}

func ParseLdapUri(uri string) (config LDAPConfig, err error) {
	uri, err = url.QueryUnescape(uri)
	if err != nil {
		return config, err
	}
	parsedURL, err := url.Parse(uri)
	if err != nil {
		return config, err
	}
	config.Scheme = parsedURL.Scheme
	config.Host = parsedURL.Host
	config.Filter = "(objectClass=*)"
	config.DN = strings.TrimLeft(parsedURL.Path, "/")
	parts := strings.Split(parsedURL.RawQuery, "?")
	switch len(parts) {
	case 4:
		config.Extensions = strings.Split(parts[3], ",")
		fallthrough
	case 3:
		if len(parts[2]) > 0 {
			config.Filter = "(" + parts[2] + ")"
		}

		fallthrough
	case 2:
		var ok bool
		config.Scope, ok = scopeMatch[parts[1]]
		if !ok {
			config.Scope = -1
		}
		fallthrough
	case 1:
		config.Attributes = strings.Split(parts[0], ",")
	case 0:
		break
	default:
		return config, fmt.Errorf("too many querys")
	}
	return config, nil
}

func GetLdapFile(uri string) ([]byte, error) {
	config, err := ParseLdapUri(uri)
	if err != nil {
		return nil, err
	}
	conn, err := ldap.DialURL(config.Scheme + "://" + config.Host)
	if err != nil {
		return nil, err
	}
	defer conn.Close()
	request := ldap.NewSearchRequest(config.DN, config.Scope, ldap.NeverDerefAliases, 0, 0, false, config.Filter, config.Attributes, nil)
	sr, err := conn.Search(request)
	if err != nil {
		return nil, err
	}
	for _, entry := range sr.Entries {
		for _, attr := range entry.Attributes {
			if attr.Name != "DN" && len(attr.ByteValues) > 0 {
				return attr.ByteValues[0], nil
			}
		}
	}
	return nil, fmt.Errorf("empty response")
}
