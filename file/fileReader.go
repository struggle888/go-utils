package file

import (
	"bufio"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"github.com/rotisserie/eris"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"strings"
)

// FileLineHandler 对于一个文件(filename 文件路径)中的每一行 顺序执行函数中的操作（兼容大文件）
func FileLineHandler(filename string, handler func(string)) error {
	f, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer f.Close()
	sc := bufio.NewScanner(f)
	for sc.Scan() {
		handler(sc.Text())
	}
	return sc.Err()
}

// GetFileWithMd5 从fileURI请求文件并保存到本地filePath。如果filePath已存在，则计算md5并与fileURI+'.md5'的请求结果进行比较，如果相同则结束，不同（或请求失败）则请求fileURI并将结果覆盖写入到filePath
func GetFileWithMd5(filePath string, fileURI string) error {
	var reason error = eris.New("Target file not exists")
	if FilePathExists(filePath) {
		resp, err := http.Get(fileURI + ".md5")
		if err == nil {
			if resp.StatusCode/100 == 2 {
				targetHash, err := ioutil.ReadAll(resp.Body)
				defer resp.Body.Close()
				if err == nil {
					hash, err := GetFileMd5(filePath)
					if err == nil {
						if hash == strings.ToLower(string(targetHash)) {
							fmt.Printf("Same Hash, Synced: %s\n", filePath)
							return nil
						} else {
							reason = eris.New(fmt.Sprintf("Different Hash: current %s, except %s", hash, strings.ToLower(string(targetHash))))
						}
					} else {
						reason = err
					}
				} else {
					reason = err
				}

			} else {
				reason = eris.New(fmt.Sprintf("An abnormal status code was obtained when requesting hash uri[%s]: %d", fileURI+".md5", resp.StatusCode))
			}
		} else {
			reason = err
		}
	}
	fmt.Printf("Unsynced hash: %v, do sync\n", reason)
	resp, err := http.Get(fileURI)
	if err != nil {
		return err
	}
	if resp.StatusCode/100 != 2 {
		return eris.New(fmt.Sprintf("An abnormal status code was obtained when requesting fileURI[%s]: %d", fileURI, resp.StatusCode))
	}
	f, err := os.Create(filePath)
	if err != nil {
		return err
	}
	_, err = io.Copy(f, resp.Body)
	if err != nil {
		return err
	}
	_ = resp.Body.Close()
	err = f.Sync()
	if err != nil {
		return err
	}
	err = f.Close()
	if err != nil {
		return err
	}
	return nil
}

// 获取文件MD5
func GetFileMd5(filePath string) (string, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return "", err
	}
	defer file.Close()

	hash := md5.New()
	buf, reader := make([]byte, 65535), bufio.NewReader(file)
	for {
		n, err := reader.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return "", err
		}
		hash.Write(buf[:n])
	}
	return hex.EncodeToString(hash.Sum(nil)), nil
}

// 判断所给路径文件/文件夹是否存在
func FilePathExists(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true
}
