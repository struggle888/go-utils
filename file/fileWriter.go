package file

import (
	"bufio"
	"fmt"
	"github.com/rotisserie/eris"
	"os"
	"os/exec"
	"strconv"
	"time"
)

type FILEMODE uint8

var (
	NORMAL   FILEMODE = 0
	REALTIME FILEMODE = 1
)

// 文件写入工具
//
// 功能: 自动按大小拆分；自动按索引命名；写入完成后自动移动。适合写入大量数据、日志时使用。
type FileWriter struct {
	// 属性
	name string
	// 当一个文件写入完成后，会自动将其移动到该目录下，一般需要以'/'结尾，默认不移动
	FinishPathPrefix string
	// 单文件大小限制，单位字节。默认无限制
	FileSizeLimit uint64
	// 每天自动切分
	ZeroSegmentation bool
	// 文件写入模式
	FileMode FILEMODE
	// 是否在写入完成一个文件后自动压缩并删除原文件（tgz）
	AutoCompress bool
	// 当前最大的文件数目，防止写入文件过大撑满硬盘
	MaxPresentFileAmount int

	// 当前状态
	currentSize      uint64 //当前已写入文件大小
	index            int    //当前文件序号
	currentFilename  string //当前文件名
	currentCompress  uint32 //当前正在压缩的文件数
	WrittenFilenames []string

	// 动态文件指针
	writer *bufio.Writer
	file   *os.File

	//记录日期,用于零点切分
	day int

	// 生成文件名的方法，默认为DefaultGenerateFilename: [name]-[index].csv，其中index指正在写入的文件序号（因为文件大小限制会依次写入多个文件），第一个文件的index为0
	GenerateFilename func(name string, index int) string
}

// 创建一个FileWriter，指定其名称name(与命名相关), finishPathPrefix(结束后文件移动路径), fileSizeLimit(文件大小限制，单位字节), ZeroSegmentation(是否零点自动切分)
func NewFileWriter(name string) *FileWriter {
	return &FileWriter{
		// 必须配置
		name: name,
		// 默认配置
		FinishPathPrefix:     "./",
		FileSizeLimit:        0,
		GenerateFilename:     defaultGenerateFilename,
		ZeroSegmentation:     false,
		FileMode:             NORMAL, // 文件创建模式: NORMAL 正常, RealTime 实时写入硬盘
		AutoCompress:         false,  // 自动压缩
		MaxPresentFileAmount: 0,      // 默认不限制
		// 自身属性
		index:       0,
		currentSize: 0,
		writer:      nil,
		day:         time.Now().Day(),
	}
}

// 写入字符串
func (fw *FileWriter) WriteString(s string) error {
	if fw.file == nil {
		err := fw.initFileWriter()
		if err != nil {
			return err
		}
	}
	if fw.FileSizeLimit > 0 && fw.currentSize+uint64(len(s)) > fw.FileSizeLimit {
		err := fw.Close()
		if err != nil {
			return err
		}
		err = fw.initFileWriter()
		if err != nil {
			return err
		}
	}
	if fw.ZeroSegmentation && time.Now().Day() != fw.day {
		err := fw.Close()
		if err != nil {
			return err
		}
		err = fw.initFileWriter()
		if err != nil {
			return err
		}
		fw.day = time.Now().Day()
	}

	if fw.FileMode == NORMAL {
		_, err := fw.writer.WriteString(s)
		if err != nil {
			return err
		}
	} else if fw.FileMode == REALTIME {
		_, err := fw.file.WriteString(s)
		if err != nil {
			return err
		}
	}

	fw.currentSize += uint64(len(s))
	return nil
}

// 初始化文件，用于创建新的待写入文件时
func (fw *FileWriter) initFileWriter() error {
	filename := fw.GenerateFilename(fw.name, fw.index)
	if fw.MaxPresentFileAmount > 0 {
	checkFileAmount:
		var currentWrittenFilenames []string
		for i := range fw.WrittenFilenames {
			if FilePathExists(fw.WrittenFilenames[i]) {
				currentWrittenFilenames = append(currentWrittenFilenames, fw.WrittenFilenames[i])
			}
		}
		fw.WrittenFilenames = currentWrittenFilenames
		if len(fw.WrittenFilenames) >= fw.MaxPresentFileAmount {
			time.Sleep(10 * time.Second)
			goto checkFileAmount
		}
	}
	if fw.FileMode == NORMAL {
		f, err := os.Create(filename)
		if err != nil {
			return err
		}
		fw.writer = bufio.NewWriter(f)
		fw.file = f
	} else if fw.FileMode == REALTIME {
		file, err := os.OpenFile(filename, os.O_WRONLY|os.O_CREATE|os.O_SYNC|os.O_TRUNC, 0666)
		if err != nil {
			return eris.Wrapf(err, "Error in Open New File")
		}
		fw.file = file
		fw.writer = nil
	}

	fw.currentSize = 0
	fw.currentFilename = filename
	return nil
}

// 将内存数据写入文件，关闭当前文件描述符，完成写入
func (fw *FileWriter) Close() error {
	if fw.file == nil {
		return nil
	}
	if fw.FileMode == NORMAL {
		err := fw.writer.Flush()
		if err != nil {
			return err
		}
	}
	err := fw.file.Close()
	if err != nil {
		return err
	}
	err = os.Rename(fw.currentFilename, fw.FinishPathPrefix+fw.currentFilename)
	if err != nil {
		return err
	}

	if fw.AutoCompress {
		go func(filePath string) {
			fw.currentCompress++
			defer func() {
				fw.currentCompress--
			}()
			err := exec.Command("tar", "cvzf", filePath+".tgz", filePath).Run()
			if err != nil {
				fmt.Printf("Error in compress %s: %v\n", filePath, err)
				return
			}
			err = os.Remove(filePath)
			if err != nil {
				fmt.Printf("Error in remove %s: %v\n", filePath, err)
			}
			fw.WrittenFilenames = append(fw.WrittenFilenames, filePath+".tgz")
		}(fw.FinishPathPrefix + fw.currentFilename)
	} else {
		fw.WrittenFilenames = append(fw.WrittenFilenames, fw.FinishPathPrefix+fw.currentFilename)
	}
	fw.index++
	fw.writer = nil
	fw.file = nil
	return nil
}

// 关闭文件并等待压缩完成
func (fw *FileWriter) CloseWithJoin() error {
	err := fw.Close()
	if err != nil {
		return err
	}
	if fw.AutoCompress {
		for fw.currentCompress > 0 {
			time.Sleep(100 * time.Millisecond)
		}
	}
	return nil
}

// 默认的生成文件名的方法
func defaultGenerateFilename(name string, index int) string {
	return name + "-" + strconv.Itoa(index) + ".csv"
}
