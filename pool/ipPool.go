package pool

import (
	"bitbucket.org/struggle888/go-utils/tools"
	"encoding/binary"
	"fmt"
	"net"
	"sort"
	"strings"
)

type IPv4Pool struct {
	pointSet map[uint32]struct{}
	ipRange  [][2]uint32
	size     uint32
}

func NewIPv4Pool(inputs []string, usePointSet bool) (*IPv4Pool, error) {
	pool := &IPv4Pool{pointSet: make(map[uint32]struct{})}
	var size uint32
	for _, c := range inputs {
		split := strings.Index(c, "/")
		// is cidr
		if split != -1 {
			_, cidrNet, err := net.ParseCIDR(c)
			if err != nil {
				return nil, fmt.Errorf("unrecognized network string: %s", c)
			}
			ones, bits := cidrNet.Mask.Size()
			if bits-ones > 32 {
				return nil, fmt.Errorf("too large cidr size: %s", c)
			}
			size = 1 << (bits - ones)
			// is ipv6 cidr
			if cidrNet.IP.To4() == nil {
				return nil, fmt.Errorf("unsupport cidr, not support ipv6: %s", c)
			}
			pool.ipRange = append(pool.ipRange, [2]uint32{
				binary.BigEndian.Uint32(cidrNet.IP.To4()),
				binary.BigEndian.Uint32(cidrNet.IP.To4()) + size - 1,
			})
			// is ip range
		} else if strings.Index(c, "-") != -1 {
			parts := strings.Split(c, "-")
			if len(parts) != 2 {
				panic(fmt.Sprintf("Unrecognized network string: %s", c))
			}
			startIP := net.ParseIP(parts[0])
			startIPv4 := startIP.To4()
			endIP := net.ParseIP(parts[1])
			endIPv4 := endIP.To4()
			if startIP == nil || endIP == nil || (startIPv4 == nil && endIPv4 != nil) || (startIPv4 != nil && endIPv4 == nil) {
				return nil, fmt.Errorf("unrecognized network string: %s", c)
			}
			// is ipv6
			if startIPv4 == nil {
				return nil, fmt.Errorf("unsupport ip range, not support ipv6: %s", c)
			}
			start := tools.IP2Int(parts[0])
			end := tools.IP2Int(parts[1])
			if end < start {
				return nil, fmt.Errorf("unrecognized network string: %s", c)
			}
			pool.ipRange = append(pool.ipRange, [2]uint32{
				start,
				end,
			})
			// 点IP
		} else if ip := net.ParseIP(c); ip != nil {
			ip = ip.To4()
			if ip == nil {
				return nil, fmt.Errorf("unsupport ip range, not support ipv6: %s", c)
			}
			if usePointSet {
				pool.pointSet[binary.BigEndian.Uint32(ip)] = struct{}{}
			} else {
				pool.ipRange = append(pool.ipRange, [2]uint32{
					binary.BigEndian.Uint32(ip),
					binary.BigEndian.Uint32(ip),
				})
			}

		} else {
			return nil, fmt.Errorf("unrecognized network string: %s", c)
		}
	}
	sort.Slice(pool.ipRange, func(i, j int) bool {
		return pool.ipRange[i][0] < pool.ipRange[j][0]
	})
	pool.ipRange = autoMerge(pool.ipRange)
	return pool, nil
}

func (pool *IPv4Pool) Contains(ip net.IP) bool {
	n := binary.BigEndian.Uint32(ip.To4())
	if _, has := pool.pointSet[n]; has {
		return true
	}
	var low, mid, high int
	high = len(pool.ipRange) - 1

	for low <= high {
		mid = (low + high) / 2
		if pool.ipRange[mid][0] <= n {
			low = mid + 1
		} else {
			high = mid - 1
		}
	}
	if high < 0 {
		return false
	} else {
		return n <= pool.ipRange[high][1]
	}
}

func autoMerge(r [][2]uint32) [][2]uint32 {
	if len(r) < 2 {
		return r
	}
	result := [][2]uint32{r[0]}
	for _, item := range r[1:] {
		last := &result[len(result)-1]
		if item[0] <= last[1]+1 {
			if item[1] > last[1] {
				last[1] = item[1]
			}
		} else {
			result = append(result, item)
		}
	}
	return result
}
