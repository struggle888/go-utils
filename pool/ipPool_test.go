package pool

import (
	"fmt"
	"net"
	"testing"
)

func TestIPv4Pool_Contains(t *testing.T) {
	pool, err := NewIPv4Pool([]string{
		"1.2.3.4",
		"2.3.4.0/24",
		"2.3.4.0/23",
		//"1.2.3.5-1.2.3.9",
	}, true)
	if err != nil {
		panic(err)
	}
	fmt.Println(pool.Contains(net.ParseIP("1.2.3.4")))
	fmt.Println(pool.Contains(net.ParseIP("1.2.3.3")))
	fmt.Println(pool.Contains(net.ParseIP("1.2.3.5")))
	fmt.Println(pool.Contains(net.ParseIP("1.2.3.8")))
	fmt.Println(pool.Contains(net.ParseIP("2.3.5.255")))
	fmt.Println(pool.Contains(net.ParseIP("2.3.6.0")))
}
