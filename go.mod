module bitbucket.org/struggle888/go-utils

go 1.14

require (
	github.com/gin-gonic/gin v1.9.1
	github.com/go-ldap/ldap v3.0.3+incompatible
	github.com/likexian/whois v1.12.4
	github.com/likexian/whois-parser v1.22.0
	github.com/miekg/dns v1.1.43
	github.com/rotisserie/eris v0.5.1
	github.com/tidwall/gjson v1.11.0
	github.com/valyala/fasthttp v1.47.0
	github.com/wumansgy/goEncrypt v0.0.0-20210730092718-e359121aa81e
	go.mongodb.org/mongo-driver v1.7.4
	go.uber.org/zap v1.19.1
	golang.org/x/net v0.10.0
	gopkg.in/asn1-ber.v1 v1.0.0-20181015200546-f715ec2f112d // indirect
)
