package snapshot

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/tidwall/gjson"
	"github.com/valyala/fasthttp"
	"math/rand"
	"net/url"
	"time"
)

var ApiUrl string
var RequestMethod = "POST"
var RetryMaxAmount = 3

func GetSnapshot(client *fasthttp.Client, url string, apiUrls ...string) (map[string]interface{}, error) {
	if client == nil {
		client = &fasthttp.Client{}
	}
	httpRequest := fasthttp.AcquireRequest()
	defer fasthttp.ReleaseRequest(httpRequest)
	httpResponse := fasthttp.AcquireResponse()
	timeoutRetryFlag := false
	RetryAmount := 0
retry:
	httpRequest.Reset()
	if len(apiUrls) > 0 {
		httpRequest.SetRequestURI(apiUrls[rand.Intn(len(apiUrls))])
	} else {
		httpRequest.SetRequestURI(ApiUrl)
	}
	httpRequest.Header.SetContentType("application/json")
	httpRequest.Header.SetMethod(RequestMethod)
	if timeoutRetryFlag {
		b, _ := json.Marshal(map[string]interface{}{
			"url":       url,
			"embedUrls": true,
			"waitUntil": "networkidle2",
			"timeout":   60000,
		})
		httpRequest.SetBody(b)
	} else {
		b, _ := json.Marshal(map[string]interface{}{
			"url":       url,
			"embedUrls": true,
			"timeout":   20000,
		})
		httpRequest.SetBody(b)
	}
	err := client.Do(httpRequest, httpResponse)
	if err != nil {
		fasthttp.ReleaseResponse(httpResponse)
		return nil, err
	}
	if httpResponse.StatusCode() == 504 || httpResponse.StatusCode() == 432 {
		time.Sleep(time.Second)
		goto retry
	}
	if httpResponse.StatusCode() != 200 {
		if bytes.Contains(httpResponse.Body(), []byte("ERR_NAME_NOT_RESOLVED")) || bytes.Contains(httpResponse.Body(), []byte("ERR_ADDRESS_UNREACHABLE")) || bytes.Contains(httpResponse.Body(), []byte("ERR_CONNECTION_REFUSED")) || bytes.Contains(httpResponse.Body(), []byte("is unreachable")) {
			fasthttp.ReleaseResponse(httpResponse)
			return nil, fmt.Errorf("remote address unreachable")
		}
		if timeoutRetryFlag == false && bytes.Contains(bytes.ToLower(httpResponse.Body()), []byte("timeout")) {
			timeoutRetryFlag = true
		}
		if bytes.Contains(bytes.ToLower(httpResponse.Body()), []byte("Unexpected server response: 429")) || bytes.Contains(bytes.ToLower(httpResponse.Body()), []byte("Unexpected server response: 502")) {
			fmt.Println(429)
			time.Sleep(3 * time.Second)
			goto retry
		}
		if RetryAmount < RetryMaxAmount {
			RetryAmount++
			if RetryAmount == RetryMaxAmount {
				timeoutRetryFlag = true
			}
			goto retry
		}
		fasthttp.ReleaseResponse(httpResponse)
		return nil, fmt.Errorf("too many retry")
	}
	return parseHttpResponse(httpResponse), nil
}

func parseHttpResponse(response *fasthttp.Response) map[string]interface{} {
	body := gjson.ParseBytes(response.Body())
	data := make(map[string]interface{})
	data["timestamp"] = body.Get("timestamp").Int()
	remoteAddress := map[string]interface{}{
		"ip":   body.Get("remoteAddress.ip").String(),
		"port": body.Get("remoteAddress.port").Int(),
	}
	data["remote_address"] = remoteAddress
	data["request_url"] = body.Get("track.request").String()
	data["response_url"] = body.Get("track.response").String()
	data["img_url"] = body.Get("screenshotImg").String()
	if body.Get("faviconData").Exists() {
		data["faviconData"] = map[string]string{
			"favicon_img": body.Get("faviconData.faviconImg").String(),
			"md5":         body.Get("faviconData.md5Str").String(),
		}
	}

	frame := map[string]interface{}{
		"child_frames": []map[string]interface{}{},
	}
	frame["title"] = body.Get("frameStructure.0.metaResult.htmlTitle").String()
	frame["keywords"] = []string{}
	for _, word := range body.Get("frameStructure.0.metaResult.htmlKeywords").Array() {
		if word.String() != "" {
			frame["keywords"] = append(frame["keywords"].([]string), word.String())
		}
	}
	frame["description"] = []string{}
	for _, word := range body.Get("frameStructure.0.metaResult.htmlDescription").Array() {
		if word.String() != "" {
			frame["description"] = append(frame["description"].([]string), word.String())
		}
	}
	frame["text_content_url"] = body.Get("frameStructure.0.textContentUrl").String()
	frame["html_url"] = body.Get("frameStructure.0.htmlFileUrl").String()
	frame["frame_url"] = body.Get("frameStructure.0.frameUrl").String()
	frame["icp"] = body.Get("frameStructure.0.icpStr").String()
	frame["child_frames"] = convertFrame(body.Get("frameStructure.0.childFrames").Array())
	data["frame"] = frame

	externalUrlMap := make(map[string]struct{})
	for u := range body.Get("urls.external").Map() {
		uu, err := url.QueryUnescape(u)
		if err != nil {
			uu = u
		}
		externalUrlMap[uu] = struct{}{}
	}
	externalUrl := []string{}
	for key := range externalUrlMap {
		externalUrl = append(externalUrl, key)
	}
	data["external_urls"] = externalUrl

	if body.Get("securityDetails").Exists() {
		cert := map[string]interface{}{
			"hash": body.Get("securityDetails.certHash").String(),
			"raw":  body.Get("securityDetails.certRaw").String(),
		}
		sanDomains := []string{}
		for _, domain := range body.Get("securityDetails.sanList").Array() {
			sanDomains = append(sanDomains, domain.String())
		}
		cert["san_dns_names"] = sanDomains
		data["cert"] = cert
	}

	return data
}

func convertFrame(frames []gjson.Result) []map[string]interface{} {
	result := []map[string]interface{}{}
	for _, frame := range frames {
		result = append(result, map[string]interface{}{
			"text_content_url": frame.Map()["textContentUrl"].String(),
			"html_url":         frame.Map()["htmlFileUrl"].String(),
			"frame_url":        frame.Map()["frameUrl"].String(),
			"child_frames":     convertFrame(frame.Map()["childFrames"].Array()),
		})
	}
	return result
}
