package snapshot

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestMainDomain(t *testing.T) {
	fmt.Println(GetMainDomain("a.baidu.com"))
	fmt.Println(GetMainDomain("a.hitwh.edu.cn"))
	fmt.Println(GetMainDomain("net.cn"))
	fmt.Println(GetMainDomain("hitwh.net.cn"))
	fmt.Println(GetMainDomain("cn"))
	fmt.Println(GetMainDomain("12d21"))
}

func TestSnapshot(t *testing.T) {
	result, err := GetSnapshot(nil, "http://www.baidu.com")
	if err != nil {
		panic(err)
	}
	b, _ := json.Marshal(result)
	fmt.Println(string(b))
}
