package main

import (
	"fmt"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var logger = initLogger()

func init() {
	//rand.Seed(time.Now().UnixNano())
}

/*
生产者消费者核心逻辑

           /---> ConsumerA ---\
Producer ------> ConsumerB -----> OutputHandler
           \---> ConsumerC ---/

*/
func run() {
	/* 初始化参数 */
	logger.Info("初始化中...")
	workerAmount := 100 // 并发worker数量

	/* 初始化管道 */
	inputChannel := make(chan string, 100*(workerAmount)) // 输入管道
	outputChannel := make(chan string, 10*(workerAmount)) // 结果输出管道
	finishChannel := make(chan bool, (workerAmount)+1)    // 结束信息管道

	/* 启动 */
	logger.Info("启动~")
	go producer(inputChannel)                      // 启动数据输入
	go outputHandler(outputChannel, finishChannel) // 启动数据输出处理协程
	// 启动对应数量的worker
	for i := 0; i < workerAmount; i++ {
		go consumer(inputChannel, outputChannel, finishChannel)
	}

	/* 等待输入管道关闭、worker完成全部任务 */
	for i := 0; i < workerAmount; i++ {
		<-finishChannel
	}
	logger.Info("worker已完成全部任务")
	close(outputChannel) // 关闭输出管道，通知输出处理协程已无新输出

	/* 等待输入管道关闭 */
	logger.Info("等待剩余结果写入完成")
	<-finishChannel //输出处理协程结束

	/* 结束 */
	close(finishChannel)
	logger.Info("完成！")
}

// 消费者
func consumer(input <-chan string, output chan<- string, finish chan<- bool) {
	for item := range input {
		output <- item + "ok!"
	}

	// 运行结束后通知【必要】
	finish <- true
}

// 示例生产者, 函数参数可自定义
func producer(input chan<- string) {
	/* 自定义操作，如：*/
	for i := 0; i < 10; i++ {
		input <- "test"
	}

	//输入结束后关闭输入管道【必要】
	close(input)
}

// 示例输出处理函数, 函数参数可自定义
func outputHandler(output <-chan string, finish chan<- bool) {
	/* 自定义操作，如：*/
	for item := range output {
		fmt.Println(item)
	}

	//输出结束后通知
	finish <- true
}

func main() {
	run()
}

// 初始化日志模块，日志第三方库选择可以自行更改
func initLogger() *zap.SugaredLogger {
	cfg := zap.NewProductionConfig()
	cfg.EncoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	cfg.Level.SetLevel(zapcore.InfoLevel)
	logger, _ := cfg.Build()
	defer logger.Sync() // flushes buffer, if any
	return logger.Sugar()
}
